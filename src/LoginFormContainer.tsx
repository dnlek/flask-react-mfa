import React, { useState, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { login, getLoginError, clearLoginError } from "./store/app";
import LoginForm from "./LoginForm";

function LoginFormContainer() {
  const dispatch = useDispatch();
  const error = useSelector(getLoginError);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const onLoginSubmitted = useCallback(
    async (evt) => {
      evt.preventDefault();
      dispatch(login({ email, password }));
    },
    [email, password, dispatch]
  );

  return (
    <LoginForm
      error={error}
      clearError={() => dispatch(clearLoginError())}
      email={email}
      password={password}
      onEmailChanged={(v: string) => setEmail(v)}
      onPasswordChanged={(v: string) => setPassword(v)}
      onLogin={onLoginSubmitted}
    />
  );
}

export default LoginFormContainer;
