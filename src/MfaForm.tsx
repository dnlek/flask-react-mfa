import React, { FormEventHandler } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import classes from "./LoginForm.module.css";

interface Props {
  error?: string;
  clearError: Function;
  code: string;
  onCodeChanged: Function;
  onSubmit: FormEventHandler<HTMLFormElement>;
}

function LoginForm({
  error,
  clearError,
  code,
  onCodeChanged,
  onSubmit,
}: Props) {
  return (
    <div className={classes.paper}>
      <Avatar className={classes.avatar}>
        <LockOutlinedIcon />
      </Avatar>
      <Typography component="h1" variant="h5">
        Two Factor auth
      </Typography>
      <form className={classes.form} onSubmit={onSubmit}>
        <TextField
          type="text"
          variant="outlined"
          margin="normal"
          required
          fullWidth
          label="Code"
          name="code"
          autoFocus
          value={code}
          onChange={(evt) => onCodeChanged(evt.target.value)}
        />
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
        >
          Sign In
        </Button>
      </form>
      <Snackbar open={!!error} autoHideDuration={6000} onClose={() => clearError()}>
        <Alert onClose={() => clearError()} severity="error">{error}</Alert>
      </Snackbar>
    </div>
  );
}

export default LoginForm;
