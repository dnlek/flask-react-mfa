import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import type { RootState } from "./store";

export interface User {
  name: string;
  email: string;
  mfa: boolean;
}

// Define a type for the slice state
interface AppState {
  isCheckingUser: boolean;
  user?: User;
  isMfa: boolean;
  loginError?: string;
}

const initialState = {
  isCheckingUser: true,
  user: undefined,
  isMfa: false,
  loginError: undefined,
} as AppState;

export const checkUser = createAsyncThunk<User>("checkUser", async () => {
  const resp = await fetch("/api/me");
  if (!resp.ok) {
    throw new Error();
  }
  return (await resp.json()) as User;
});

export const logout = createAsyncThunk("logout", async () => {
  const resp = await fetch("/api/logout");
  if (!resp.ok) {
    throw new Error();
  }
});

interface LoginParams {
  email: string;
  password: string;
}

export const login = createAsyncThunk<User, LoginParams, {}>(
  "login",
  async (loginParams) => {
    const resp = await fetch("/api/login", {
      method: "POST",
      body: JSON.stringify({
        email: loginParams.email,
        password: loginParams.password,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (resp.ok) {
      return (await resp.json()) as User;
    } else {
      throw new Error("Invalid login or password");
    }
  }
);

export const submitMfaCode = createAsyncThunk<User, string, {}>(
  "submitMfaCode",
  async (code) => {
    const resp = await fetch("/api/mfa", {
      method: "POST",
      body: JSON.stringify({ code }),
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (!resp.ok) {
      throw new Error();
    }
    return (await resp.json()) as User;
  }
);

export const loginSlice = createSlice({
  name: "login",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    clearLoginError: (state) => {
      state.loginError = undefined;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(checkUser.fulfilled, (state, { payload }) => {
      state.user = payload;
      state.isCheckingUser = false;
    });
    builder.addCase(checkUser.rejected, (state) => {
      state.isCheckingUser = false;
    });
    builder.addCase(logout.fulfilled, (state) => {
      state.user = undefined;
    });
    builder.addCase(logout.rejected, (state) => {
      state.user = undefined;
    });
    builder.addCase(login.pending, (state) => {
        state.loginError = undefined
    });
    builder.addCase(login.fulfilled, (state, { payload }) => {
      if (payload.mfa) {
        state.isMfa = true;
      } else if (payload) {
        state.user = payload;
        state.isMfa = false;
      }
    });
    builder.addCase(login.rejected, (state) => {
      state.isMfa = false;
      state.loginError = "Invalid login or password";
    });
    builder.addCase(submitMfaCode.pending, (state) => {
        state.loginError = undefined
    });
    builder.addCase(submitMfaCode.fulfilled, (state, { payload }) => {
      state.user = payload
      state.isMfa = false
    });
    builder.addCase(submitMfaCode.rejected, (state) => {
      state.loginError = "Invalid code";
    });
  },
});

export const { clearLoginError } = loginSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const getIsCheckingUser = (state: RootState) => state.app.isCheckingUser;
export const getUser = (state: RootState) => state.app.user;
export const getIsMfa = (state: RootState) => state.app.isMfa;
export const getLoginError = (state: RootState) => state.app.loginError;

export default loginSlice.reducer;
