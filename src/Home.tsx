import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Link from "@material-ui/core/Link";
import UserIcon from "@material-ui/icons/Person";
import Typography from "@material-ui/core/Typography";
import classes from "./Home.module.css";

interface Props {
  user: any,
  onLogoutClick: Function,
}

function LoginForm({
  user,
  onLogoutClick,
}: Props) {
  return (
    <div className={classes.paper}>
      <Avatar className={classes.avatar}>
        <UserIcon />
      </Avatar>
      <Typography component="h1" variant="h5">
        Welcome {user?.name}
      </Typography>
      <Link component="button" onClick={() => onLogoutClick()}>Logout</Link>
    </div>
  );
}

export default LoginForm;
