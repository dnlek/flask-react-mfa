import React, { useState, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import MfaForm from "./MfaForm";
import { submitMfaCode, getLoginError, clearLoginError } from "./store/app";

function MfaFormContainer() {
  const dispatch = useDispatch();
  const error = useSelector(getLoginError);
  const [code, setCode] = useState("");

  const onMfaSubmitted = useCallback(
    async (evt) => {
      evt.preventDefault();
      dispatch(submitMfaCode(code));
    },
    [code, dispatch]
  );

  return (
    <MfaForm
      error={error}
      clearError={() => dispatch(clearLoginError())}
      code={code}
      onCodeChanged={(v: string) => setCode(v)}
      onSubmit={onMfaSubmitted}
    />
  );
}

export default MfaFormContainer;
