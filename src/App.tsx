import React, { useCallback, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux"
import Container from "@material-ui/core/Container";
import CircularProgress from "@material-ui/core/CircularProgress"
import LoginFormContainer from "./LoginFormContainer";
import MfaFormContainer from "./MfaFormContainer";
import Home from "./Home";
import { checkUser, getIsCheckingUser, getIsMfa, getUser, logout } from "./store/app";
import classes from "./App.module.css";

function App() {
  const dispatch = useDispatch()
  const isCheckingUser = useSelector(getIsCheckingUser)
  const user = useSelector(getUser)
  const isMfa = useSelector(getIsMfa)

  useEffect(() => {
    dispatch(checkUser())
  }, [dispatch])

  const onLogoutClick = useCallback(async () => {
    dispatch(logout())
  }, [dispatch])

  return (
    <Container component="main" maxWidth="xs">
      {!!isCheckingUser && <CircularProgress className={classes.loading} />}
      {!isCheckingUser && (
        <React.Fragment>
          {!!user && (<Home user={user} onLogoutClick={onLogoutClick} />)}
          {!user && !isMfa && <LoginFormContainer /> }
          {!user && !!isMfa && <MfaFormContainer /> }
        </React.Fragment>
      )}
    </Container>
  );
}

export default App;
