# Preview

[Video](./video.mov)

# How to run

Create virtualenv, pipenv, pyenv, etc...

Install python requirements

`pip install -r requirements.txt`

Install javascript requirements

`yarn install`

Run in dev mode

`yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

# Users

```
login: test1@example.com
password: test123
otp: 1111


login: test2@example.com
password: test123
mfa not enabled
```