import time
import re
from flask import Flask, session, request, abort, redirect, url_for

email_regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'

USERS=[
    { 'name': 'John Doe', 'email': 'test1@example.com', 'password': 'test123', 'mfa': True },
    { 'name': 'Ben Test', 'email': 'test2@example.com', 'password': 'test123', 'mfa': False },
]

app = Flask(__name__, static_folder='./build', static_url_path='/')
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

@app.route('/')
def index():
    return app.send_static_file('index.html')

@app.route('/api/logout', methods=['GET'])
def logout():
    session.pop('user')
    # return redirect(url_for('index'))
    return {}

@app.route('/api/me', methods=['GET'])
def me():
    if session.get('user') is not None:
        return { 'email': session['user'].get('email'), 'name': session['user'].get('name') }
    else:
        abort(401)

@app.route('/api/login', methods=['POST'])
def login():
    request_data = request.get_json()
    email = request_data['email']
    password = request_data['password']
    
    if not re.search(email_regex, email):
        abort(400)

    user = next((u for u in USERS if u['email'] == email), None)

    if user is None:
        abort(401)

    if user.get('password') != password:
        abort(401)

    if not user['mfa']:
        session['user'] = user
        return {'email': email, 'name': user.get('name') }
    else:
        session['mfa_user'] = user
        return {'mfa': True, 'email': email, 'name': user.get('name') }

@app.route('/api/mfa', methods=['POST'])
def mfa():
    request_data = request.get_json()
    code = request_data['code']

    if not 'mfa_user' in session:
        abort(401)

    if code == '1111':
        user = session.pop('mfa_user')
        session['user'] = user
        return {'email': user.get('email'), 'name': user.get('name') }
    else:
        abort(401)

if __name__ == '__main__':
    app.run(debug=True)